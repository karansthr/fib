import time

def fibonacci(n: int) -> int:
    assert isinstance(n, int), 'Please enter an integer number'
    assert n > 0, 'Please enter a positive integer greater than 0'

    v1, v2, v3 = 1, 1, 0
    for rec in bin(n)[3:]:
        calc = v2*v2
        v1, v2, v3 = v1 * v1 + calc, ( v1 + v3 ) * v2, calc + v3 * v3
        if rec == '1':    
            v1, v2, v3 = v1+v2, v1, v2
    return v2


def nth_fibonacci_with_calcuation_time(number):
    start_time = time.time()
    result = fibonacci(number)
    finish_time = time.time()
    calculation_time = finish_time - start_time
    return result, calculation_time
