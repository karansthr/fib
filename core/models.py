from django.db import models


class FibonacciNthTerm(models.Model):
    number = models.IntegerField(unique=True)
    computation_time = models.TextField() # in serconds
    value = models.TextField()  # numberTH term in fibonacci sequence (1, 1, 2, ...)

    def __str__(self):
        return str(self.number)
