from django.contrib import admin
from .models import FibonacciNthTerm

admin.site.register(FibonacciNthTerm)
