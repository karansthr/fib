from django.shortcuts import redirect
from django.views.generic import TemplateView
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.generics import RetrieveAPIView

from .utils import nth_fibonacci_with_calcuation_time
from .models import FibonacciNthTerm
from .serializers import FibonacciSerializer


class FibonacciAPIView(APIView):

    def get(self, request):
        serializer = FibonacciSerializer(data=request.GET)
        if serializer.is_valid():
            data = serializer.execute(serializer.validated_data)
            return Response(
                {
                    'computation_time': f'{data.get("computation_time")} second', 
                    'value': str(data.get('value'))
                }
            )
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )


class FibonacciFastAPIView(APIView):

    def get(self, request):
        try:
            number = int(request.GET.get('number'))
        except KeyError:
            return Response(
                {'error': 'Please provide the number'},
                status=status.HTTP_400_BAD_REQUEST
            )
        except (ValueError, TypeError):
            return Response(
                {'error': 'Please enter a valid integer number'},
                status=status.HTTP_400_BAD_REQUEST
            )

        if number <= 0:
            return Response(
                {'error': 'Please enter interger greater than 0'},
                status=status.HTTP_400_BAD_REQUEST
            )
        value, computation_time = nth_fibonacci_with_calcuation_time(number)
        return Response(
            {
                'computation_time': f'{computation_time} second', 
                'value': str(value)
            }
        )


class HomeView(TemplateView):
    template_name = 'core/home.html'


class FasterHomeView(TemplateView):
    template_name = 'core/home.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['faster'] = True
        return context


class DeleteCacheView(APIView):

    def get(self, request):
        FibonacciNthTerm.objects.all().delete()
        return Response({'message': 'Cache deleted successfully'})
