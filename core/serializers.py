import time

from rest_framework import serializers

from .models import FibonacciNthTerm


class FibonacciSerializer(serializers.Serializer):
    number = serializers.IntegerField()
    computation_time = serializers.CharField(required=False)
    value = serializers.CharField(required=False)

    def validate(self, data):
        try:
            number = int(data.get('number'))
        except (ValueError, TypeError):
            raise serializers.ValidationError('Please enter a valid integer number')
        if number < 1:
            raise serializers.ValidationError('Please enter a positive integer greater than 0')
        return data

    def execute(self, validated_data):
        number = validated_data.pop('number')
        try:
            fib_nth_term_object = FibonacciNthTerm.objects.get(number=number)
            return type(self)(fib_nth_term_object).data
        except FibonacciNthTerm.DoesNotExist:
           return self.calculate_and_store_fib_value_and_calc_time(number)
    
    def calculate_and_store_fib_value_and_calc_time(self, number):

        def calcuate_fibonacci(number):
            if number in (1, 2):
                return 1
            try:
                fib_nth_term_object = FibonacciNthTerm.objects.get(number=number)
                return int(fib_nth_term_object.value)
            except FibonacciNthTerm.DoesNotExist:
                return calcuate_fibonacci(number - 1) + calcuate_fibonacci(number - 2)

        start_time = time.time()
        result = calcuate_fibonacci(number)
        finish_time = time.time()
        computation_time = finish_time - start_time
        fib_nth_term_object = FibonacciNthTerm.objects.create(
            number=number,
            value=result,
            computation_time=computation_time
        )
        return type(self)(fib_nth_term_object).data
