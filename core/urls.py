from django.urls import path

from . import views


urlpatterns = [
    path('', views.HomeView.as_view(), name='home'),
    path('delete_cache', views.DeleteCacheView.as_view(), name='delete_cache'),
    path('faster', views.FasterHomeView.as_view(), name='faster_home'),
    path(
        'api/v1/fibonacci/', 
        views.FibonacciAPIView.as_view(), 
        name='fibonacci'
    ),
    path(
        'api/v1/fibonacci/faster/', 
        views.FibonacciFastAPIView.as_view(), 
        name='faster_fibonacci'
    )
]
