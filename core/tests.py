from django.test import TestCase, Client

from .utils import fibonacci
from .models import FibonacciNthTerm

class FibonacciTestCase(TestCase):

    def setUp(self):
        self.client = Client()

    def test_fibonacci_function(self):
        self.assertEqual(fibonacci(1), 1)
        self.assertEqual(fibonacci(2), 1)
        self.assertEqual(fibonacci(3), 2)
        self.assertEqual(fibonacci(4), 3)
        self.assertEqual(fibonacci(5), 5)
        self.assertEqual(fibonacci(6), 8)
        self.assertEqual(fibonacci(7), 13)
        self.assertEqual(fibonacci(8), 21)
        self.assertEqual(fibonacci(9), 34)
        self.assertEqual(fibonacci(10), 55)
        with self.assertRaises(AssertionError):
            fibonacci(0)
        with self.assertRaises(AssertionError):
            fibonacci(-1)
        with self.assertRaises(AssertionError):
            fibonacci('fsd')
        with self.assertRaises(AssertionError):
            fibonacci(None)
    
    def get_data(self, number, faster=False):
        endpoint = '/api/v1/fibonacci/'
        if faster:
            endpoint += 'faster/'
        response = self.client.get(endpoint, {'number': number})
        return response.data
    
    def test_faster_api(self):
        self.assertEqual(self.get_data(1, faster=True).get('value'), '1')
        self.assertEqual(self.get_data(2, faster=True).get('value'), '1')
        self.assertEqual(self.get_data(3, faster=True).get('value'), '2')
        self.assertEqual(self.get_data(4, faster=True).get('value'), '3')
        self.assertEqual(self.get_data(5, faster=True).get('value'), '5')
        self.assertEqual(self.get_data(6, faster=True).get('value'), '8')
        self.assertEqual(self.get_data(7, faster=True).get('value'), '13')
        self.assertEqual(self.get_data(8, faster=True).get('value'), '21')
        self.assertEqual(self.get_data(9, faster=True).get('value'), '34')
        self.assertEqual(self.get_data(10, faster=True).get('value'), '55')
        with self.assertRaises(AssertionError):
            fibonacci(0)
        with self.assertRaises(AssertionError):
            fibonacci(-1)
        with self.assertRaises(AssertionError):
            fibonacci('fsd')
        with self.assertRaises(AssertionError):
            fibonacci(None)

    def test_api(self):
        self.assertEqual(self.get_data(1).get('value'), '1')
        self.assertEqual(self.get_data(2).get('value'), '1')
        self.assertEqual(self.get_data(3).get('value'), '2')
        self.assertEqual(self.get_data(4).get('value'), '3')
        self.assertEqual(self.get_data(5).get('value'), '5')
        self.assertEqual(self.get_data(6).get('value'), '8')
        self.assertEqual(self.get_data(7).get('value'), '13')
        self.assertEqual(self.get_data(8).get('value'), '21')
        self.assertEqual(self.get_data(9).get('value'), '34')
        self.assertEqual(self.get_data(10).get('value'), '55')
        with self.assertRaises(AssertionError):
            fibonacci(0)
        with self.assertRaises(AssertionError):
            fibonacci(-1)
        with self.assertRaises(AssertionError):
            fibonacci('xyz')
        with self.assertRaises(AssertionError):
            fibonacci(None)

        for i in range(1, 11):
            self.assertTrue(FibonacciNthTerm.objects.filter(number=i).exists())
