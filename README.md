Hosted at: https://fibwebapp.herokuapp.com/

Local set up steps:

- Clone the repo 
`git clone https://gitlab.com/karansthr/fib/`

- cd into fib
- create virtualenv
`python -m venv venv`

- activate virtual environment
`source venv/bin/activate`

- Install dependencies
`pip install -r requirements.txt`

- Create a file named .env in config directory
Content of file
```
SECRET_KEY=----valid--secret--key--you-can-generate-it-using https://www.miniwebtool.com/django-secret-key-generator/
ALLOWED_HOSTS=*
DEBUG=True
DATABASE_URL=sqlite:///db.sqlite3

```

- Database migration
`python manage.py migrate`

- Run it locally
`python manage.py runserver`